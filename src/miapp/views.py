from datetime import datetime

from django.contrib.auth.decorators import login_required, user_passes_test
from django.http import HttpResponse, JsonResponse, Http404
from django.shortcuts import render
from django.utils.timezone import now

from miapp.forms import MiContacto
import csv

from django.views.decorators.cache import cache_page


def miindex(request):
    if request.method == "POST":
        form = MiContacto(request.POST)
        if form.is_valid():
            print("wii!")
    else:
        form = MiContacto()

    context = {
        "hora": now(),
        "form": form
    }
    return render(request, "index.html", context)


@cache_page(60 * 2)
def mi_vista_cacheada(request):
    hora = now()
    return HttpResponse(str(hora))


def check_user(user):
    return user.has_perm("auth.delete_group")


@login_required
# @user_passes_test(check_user)
@user_passes_test(lambda user: user.has_perm("auth.delete_group"))
def mi_vista_atorizada(request):
    data = {
        'user': request.user.username
    }
    return JsonResponse(data)


def validar(numero):
    try:
        return int(numero)
    except:
        return -1


def mis_fechas(request, anio, mes, dia):
    hora = request.GET.get("hora", 0)
    minuto = request.GET.get("minuto", 0)

    meses = {
        "enero": 1,
        "febrero": 2,
        "marzo": 3
    }

    anio = validar(anio)
    dia = validar(dia)

    mes_p = validar(mes)

    if mes_p == -1:
        mes_p = meses[mes]

    try:
        data = {
            'anio': anio,
            'mes': meses[mes],
            'dia': dia,
            'fecha': datetime(year=anio, month=mes_p, day=dia),
            "hora": hora,
            "minuto": minuto
        }
    except:
        raise Http404("Error creating the 'fecha' field")

    return JsonResponse(data)


def descargue_csv(request):
    request = HttpResponse(content_type='text/csv')
    request['Content-Disposition'] = 'attachment; filename="somefilename.csv"'

    writer = csv.writer(request)
    writer.writerow(['First row', 'Foo', 'Bar', 'Baz'])
    writer.writerow(['Second row', 'A', 'B', 'C', '"Testing"', "Here's a quote"])

    return request
