
from django import forms

class MiContacto(forms.Form):
    nombre = forms.CharField(max_length=250)
    email = forms.EmailField()
    mensaje = forms.CharField(widget=forms.Textarea)


    def clean_nombre(self):
        """clean_campo para un campo especifico"""
        letra = self.cleaned_data["nombre"][0]
        if letra.lower() != "a":
            raise forms.ValidationError("El nombre no empieza con a")
        return self.cleaned_data["nombre"]


    def clean(self):
        """clean para todos los campos del formulario"""
        super(MiContacto, self).clean()
        data = self.cleaned_data
        if "nombre" in data and "email" in data:
            nombre = self.cleaned_data["nombre"].split(" ")[0]
            email = self.cleaned_data["email"].split("@")[0]

            if nombre.lower() != email.lower():
                raise forms.ValidationError("NO el correo no es igual nombre")
